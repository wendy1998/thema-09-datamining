import os

with open("horse-colic.data") as origin:
    with open("temp.txt", "w") as temp:
        for line in origin:
            temp.write(" ".join(line.strip().split()[:28]))
            temp.write("\n")

os.rename("temp.txt", "horse-colic.data")
            
